package ShieldMatHooker;
use Mojo::Base 'Mojolicious';
use Mojo::Util qw(url_escape encode);

sub startup {
    my $self = shift;

    $self->plugin('Config');

    $self->plugin('DebugDumperHelper');

    # Router
    my $r = $self->routes;

    # Normal route to controller
    $r->any('/' => sub {
        my $c = shift;
        my $text = $c->param('text');
        if ($c->req->method eq 'GET') {
            my @params = split('&', $c->req->content->asset->slurp);
            for my $param (@params) {
                my @h = split('=', $param);
                $c->param($h[0] => $h[1]);
                $text = $h[1] if ($h[0] eq 'text');
            }
        }
        my ($token, $channel_id, $channel_name, $command, $team_domain, $team_id, $user_id, $user_name, $response_url) =
           ($c->param('token'), $c->param('channel_id'), $c->param('channel_name'), $c->param('command'), $c->param('team_domain'), $c->param('team_id'), $c->param('user_id'), $c->param('user_name'), $c->param('response_url'));
        # This is only to check that the request comes (apparently) from mattermost
        # I know that I should test the token, but it would prevent other users from
        # framateam to use this service
        # To enable token authent, just set a token in the configuration file
        if ((!defined($c->config('token')) &&
             defined($token) &&
             defined($channel_id) &&
             defined($channel_name) &&
             defined($command) &&
             defined($team_domain) &&
             defined($team_id) &&
             defined($user_id) &&
             defined($user_name) &&
             defined($response_url))
             || (defined $c->config('token') && $token eq $c->config('token'))
        ) {
            my @array = split(' ', $text);
            my $color = pop @array;
            my $value = pop @array;
            my $label = "@array";
            $label =~ s/-/--/g;
            $value =~ s/-/--/g;
            return $c->render(
                json => {
                    "response_type" => "in_channel",
                    "text"          => sprintf('https://raster.shields.io/badge/%s-%s-%s.png', url_escape(encode 'UTF-8', $label), url_escape(encode 'UTF-8', $value), url_escape($color))
                }
            );
        } else {
            return $c->render(json => "Bad request");
        }
    });
}

1;
