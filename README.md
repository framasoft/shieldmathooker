[![](https://framagit.org/assets/gitlab_logo-cdf021b35c4e6bb149e26460f26fae81e80552bc879179dd80e9e9266b14e894.png)](https://framagit.org)

![English:](https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/20px-Flag_of_the_United_Kingdom.svg.png) **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

![Français :](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/20px-Flag_of_France.svg.png) **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# Shield Mat Hooker

Powered By Shield

The `Mat` stand for [Mattermost](http://www.mattermost.org/).

This service allows you to create [slash commands](http://docs.mattermost.com/developer/slash-commands.html) on Mattermost to get badges from https://shields.io/.

## License

Shield Mat Hooker is licensed under the terms of the AGPLv3. See the LICENSE file.

# Installation

## Dependencies

* Some packages

```shell
apt-get install build-essential libssl-dev
```

* Carton : Perl dependencies manager, it will get what you need, so don't bother for dependencies (but you can read the file `cpanfile` if you want).

```shell
sudo cpan Carton
```

## The real installation
After installing Carton :

```shell
git clone https://framagit.org/framasoft/shieldmathooker.git
cd shieldmathooker
carton install
cp shield_mat_hooker.conf.template shield_mat_hooker.conf
vi shield_mat_hooker.conf
```

The configuration file is self-documented.

## Usage

### Launch manually

This is good for test, not for production.

```
carton exec hypnotoad script/shield_mat_hooker
# stop it
carton exec hypnotoad -s script/shield_mat_hooker
```

Yup, that's all, it will listen at "http://127.0.0.1:8080".

For more options (interfaces, user, etc.), change the configuration in `shield_mat_hooker.conf` (have a look at http://mojolicio.us/perldoc/Mojo/Server/Hypnotoad#SETTINGS for the available options).

### Systemd

```
sudo su
cp utilities/shieldmathooker.service /etc/systemd/system/
vi /etc/systemd/system/shieldmathooker.service
systemctl daemon-reload
systemctl enable shieldmathooker.service
systemctl start shieldmathooker.service
```

## Nginx

You may want to put SMH behind a reverse proxy. Have a look at `utilities/shieldmathooker.nginx` for an example.

# How to use in Mattermost?

First, have a look at the [Mattermost documentation about slash commands](http://docs.mattermost.com/developer/slash-commands.html).

Then, create your slash command, using the URL of your installation (say https://shield.example.org), choose `GET` or `POST` method, it doesn't matter, SMH will answer to both.

Finally, put the token provided by Mattermost in your SMH configuration file. Or not, if you want anybody to be able to use it if he provides the mattermost parameters.

Now, you can get badges from Shield with `/badge foo bar green` (if you choose `badge` as your slash command).

Note that only the first term may contain spaces.
